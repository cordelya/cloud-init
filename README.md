# cloud-config configs

These are built for and tested with Multipass. 

## Background

*notes for myself as much as for anyone else*

When you need a VM for a specific purpose, you can pass a config written in YAML to Multipass as an argument to `multipass launch`.

The config allows you to specify whether to auto-upgrade default packages, install new packages, and set the language and time zone. You are also able to write entire files (the `write_files` stage) to temporary locations and then copy them to where they will ultimately live during the `runcmd` stage. During the `runcmd` stage, you are able to pass any cli command available, including running scripts (that you either wrote during the `write files` stage or curl from elsewhere), passing non-interactive commands to applications like `mysql`, downloading and extracting files and archives from elsewhere, and enabling services.
The `runcmd` stage by default executes commands using `root`. If you are creating files or directories that need to ultimately be owned by a different user, `chmod` them to the correct ownership near the end of the `runcmd` list.

Multipass does not support supplying multiple config files even though cloud-init does (qualification: for sure applicable to MP running on Windows. Not sure about MP running on MacOS or any Linux/Unix-like environment). Some of the files here contain `import:` phases. You need to replace those with the contents of the matching files, merging the sections by hand so there's only one top-level section of each name/type. 

## Config-Specific Notes

### WordPress
wordpress.yaml

* this file has some commented lines that need to be edited and uncommented before you can use it to launch a WordPress VM.
  * Lines 176-179 (database & db user creation): replace `[database]`, `[database-user]`, and `[database-user-password]` with your chosen database name, db user name, and db user pass
  * Lines 194-179 (`sed` wp-config.php to insert db credentials): replace `[database]`, `[database-user]`, and `[database-user-password]` with the same information as you specified in lines 176-179

* write_files section notes
  * wordpress.conf: this is the Apache2 config file for the WordPress instance. It tells Apache2 where WordPress's directory is located (because it isn't in /var/www) and sets necessary options for WordPress to function properly.
  * wp-config-0.txt and wp-config-2.txt: these are the first half and second half of the default wp-config.php file for WordPress, excluding the salts section, which is generated during the `runcmd` phase using WordPress.org's randon salt generator tool and saved as wp-config-1.txt. All 3 files are then concatenated and saved as wp-config.php in the WordPress root directory. If the default wp-config.php file as supplied by WordPress.org changes, these two sections will need to be updated with the relevant portions of the updated file.
    * You may also customize wp-config.php at this point by inserting custom config lines in the area designated for developers (near the end of the 2nd section)



